/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConexionSQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Paulino Zelaya
 */
public class conexionsql {
        //Cadena de texto que me guarde el nombre de mi base de datos
    String nombre_base_de_datos = "PANADERIA";

    //Cadena de texto que me guarde el usuario de sql server
    String usuario = "PANADERIA";

    //Cadena de texto que me guarde la contraseña de sql server
    String contraseña = "1234";

    //Cadena de texto que me guarde el nombre del driver de sql server

    String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

    //Cadena de texto que me enrute la conexion entre java y sql server
   
    String url = "jdbc:sqlserver://localhost:1433;databaseName=" + nombre_base_de_datos + ";integratedSecurity=false";

    //Connection = es para iniciar la conexion entre la base de datos y el java, por medio del driver sql
 
    Connection connection = null;

    //Constructor de la clase ConexionSQLServer
    public conexionsql() {
        try {
         
            Class.forName(driver);

          
            connection = DriverManager.getConnection(url, usuario, contraseña);

        
            if (connection != null) {
               
            }
        } catch (ClassNotFoundException | SQLException e) {
          
            System.out.println(e.getMessage());
        }
    }

    
    public Connection getConnection() {
        return connection;
    }

    //Cierra la conexion entre java y sql al finalizar una operacion.
    public void desconectar() {
        try {
            //Cerramos la conexion con el sql
            System.out.println("Cerrando Conexion Con SQL");
            connection.close();
        } catch (SQLException e) {
           
            System.out.println(e.getMessage());
        }
    }

  public static void main(String[] args) {
        try {
             conexionsql C = new conexionsql();
             C.getConnection();
             System.out.println("Conexion Exitosa");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
       
        
    }
}
