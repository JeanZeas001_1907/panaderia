/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import ConexionSQL.conexionsql;
import java.awt.Component;
import java.sql.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author HP
 */
public class CargarTablas {

    conexionsql csql = new conexionsql();
    Connection con = csql.getConnection();

    public void cargarusuarios(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM VistaUsuarios ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                model.addRow(v);

            }

        } catch (SQLException e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void ajustarcolumnas(JTable table) {
        final TableColumnModel columnModel = table.getColumnModel();
        for (int column = 0; column < table.getColumnCount(); column++) {
            int width = 15; // Min width
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width + 1, width);
            }
            if (width > 300) {
                width = 300;
            }
            columnModel.getColumn(column).setPreferredWidth(width);
        }
    }

    public void CargarClientes(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM VistaClientes ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
               
                v.add(rs.getString(6));
               String estado=rs.getString(6);
               if(estado.equals("0"))
               {
                   model.addRow(v);
               }
                   
                

            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void CargarProductos(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM VistaInventario ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getInt(3));
                v.add(rs.getInt(4));
                v.add(rs.getInt(5));
                v.add(rs.getInt(6));
                v.add(rs.getInt(7));
                model.addRow(v);

            }

        } catch (SQLException e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void InventarioBusquedaFiltro(JTable tabla, String code) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        model.setRowCount(0);

        try {
            CallableStatement cs = con.prepareCall("{call BUSCARINVENTARIOFILTRO (?)}");
            cs.setString(1, code);
            ResultSet rs = cs.executeQuery();

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getInt(3));
                v.add(rs.getInt(4));
                v.add(rs.getInt(5));
                v.add(rs.getInt(6));
                v.add(rs.getInt(7));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }

    public void CargarVentas(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM VISTAVENTAS ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getInt(2));
                v.add(rs.getInt(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                model.addRow(v);

            }

        } catch (SQLException e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void CargarClientesVentas(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM VistaClientes ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                model.addRow(v);

            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void ClienteBusquedaFiltro(JTable tabla, String code) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        model.setRowCount(0);

        try {
            CallableStatement cs = con.prepareCall("{call BUSCARCLIENTEFILTRO (?)}");
            cs.setString(1, code);
            ResultSet rs = cs.executeQuery();

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }

    public void CargarProductosVentas(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM VistaInventario ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getInt(3));
                model.addRow(v);

            }

        } catch (SQLException e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void ProductoBusquedaFiltro(JTable tabla, String code) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        model.setRowCount(0);

        try {
            CallableStatement cs = con.prepareCall("{call BUSCARPRODUCTOFILTRO (?)}");
            cs.setString(1, code);
            ResultSet rs = cs.executeQuery();

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }
    
    public void CargarClientesInactivos(JTable tabla)
    {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM VistaClientes ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                model.addRow(v);

            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
        
    }
}
