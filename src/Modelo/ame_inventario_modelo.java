/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Yader
 */
public class ame_inventario_modelo implements ActionListener {
    
    String Id, NombreProducto, precioCompra, precioVenta, CanTotal;

    public ame_inventario_modelo(String Id, String NombreProducto, String precioCompra, String precioVenta, String CanTotal) {
        this.Id = Id;
        this.NombreProducto = NombreProducto;
        this.precioCompra = precioCompra;
        this.precioVenta = precioVenta;
        this.CanTotal = CanTotal;
    }

    public ame_inventario_modelo() {
        
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getNombreProducto() {
        return NombreProducto;
    }

    public void setNombreProducto(String NombreProducto) {
        this.NombreProducto = NombreProducto;
    }

    public String getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(String precioCompra) {
        this.precioCompra = precioCompra;
    }

    public String getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(String precioVenta) {
        this.precioVenta = precioVenta;
    }

    public String getCanTotal() {
        return CanTotal;
    }

    public void setCanTotal(String CanTotal) {
        this.CanTotal = CanTotal;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            
        }
    }
    
    
}
