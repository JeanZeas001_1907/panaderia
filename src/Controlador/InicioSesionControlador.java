/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import ConexionSQL.conexionsql;
import Modelo.InicioSesionModelo;
import Vista.Administrador;
import Vista.Empleado;
import Vista.InicioSesion;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Paulino Zelaya
 */
public class InicioSesionControlador implements ActionListener {

    conexionsql cc = new conexionsql();
    Connection con = cc.getConnection();

    InicioSesion IS;
    InicioSesionModelo ISM;

    public InicioSesionControlador(InicioSesion IS) {
        super();
        this.IS = IS;
        ISM = new InicioSesionModelo();
    }

    public InicioSesionModelo getISM() {
        return ISM;
    }

    public void setISM(InicioSesionModelo ISM) {
        this.ISM = ISM;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Iniciar":
                Iniciando();
                break;
        }

    }

    public void Iniciando() {
        ISM = IS.setData();

        try {
            String sql = "EXEC INICIARSESION ?,?";

            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);

            ps.setString(1, ISM.getUsuario());
            ps.setString(2, ISM.getContraseña());

            rs = ps.executeQuery();

            if (ISM.getUsuario().isEmpty() && ISM.getContraseña().isEmpty()){
                IS.error.setText("Usuario y Contraseña vacío");
                    
            }else if        
                    (ISM.getUsuario().isEmpty()){
                    IS.error.setText("Usuario vacío");
                
            }else if
                    (ISM.getContraseña().isEmpty()){
                    IS.error.setText("Contraseña vacía");
                
            }else if
                    (rs.next()) {
                    if (rs.getString(1).equals("Administrador")) {
                        IS.dispose();
                        Administrador Ad = new Administrador();
                        Ad.setVisible(true);
                    
                    } else if (rs.getString(1).equals("Empleado")) {
                        IS.dispose();
                        Empleado Em = new Empleado();
                        Em.setVisible(true);
                    
                    }else{
                        IS.error.setText("NOMBRE DE USUARIO/CONTRASEÑA INCORRECTO");
                        
                    }
                }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            }
    }
}
