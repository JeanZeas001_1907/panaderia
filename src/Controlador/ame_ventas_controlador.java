package Controlador;

import ConexionSQL.conexionsql;
import Modelo.ame_ventas_modelo;
import Vista.AñadirVentas;
import Vista.ConsultarVentas;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class ame_ventas_controlador implements ActionListener {

    ame_ventas_modelo avm;
    AñadirVentas av;
    ConsultarVentas cv;

    conexionsql cc = new conexionsql();
    Connection con = cc.getConnection();

    public ame_ventas_controlador(AñadirVentas av) {
        super();
        this.av = av;
        avm = new ame_ventas_modelo();
    }

    public ame_ventas_controlador(ConsultarVentas cv) {
        super();
        this.cv = cv;
        avm = new ame_ventas_modelo();
    }

    public ame_ventas_modelo getAvm() {
        return avm;
    }

    public void setAvm(ame_ventas_modelo avm) {
        this.avm = avm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Xañadirventa":
                av.dispose();
                break;

            case "Xconsultarventa":
                cv.dispose();
                break;

            case "Añadir":
                añadir();
                break;

            case "Registrar Venta":
                registrarventa();
                break;

            case "Consultar":
                consulta();
                break;

            case "Xconsultar":
                cv.dispose();
                break;
        }
    }

    public void consulta() {
        avm = cv.getData();

        String numerofactura = avm.getNumerofactura();

        DefaultTableModel model = (DefaultTableModel) cv.TablaConsulta.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        cv.TablaConsulta.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "EXEC CONSULTAVENTA '" + numerofactura + "'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getString(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                model.addRow(v);

                cv.TablaConsulta.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void registrarventa(){
        ResultSet rs1 = null;
        if (av.TotalProductosTabla.getRowCount() > 0) {
            try {
                String sql = "EXEC REGISTRAR_VENTAS '" + av.TotalProductosTabla.getValueAt(0, 0) + "','" + Integer.parseInt(av.facturaField.getText()) + "'";

                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);

                if (rs.next()) {
                    String validador = rs.getString("Codigo");

                    if (validador.equals("0")) {
                        JOptionPane.showMessageDialog(null, "Ingresado Correctamente");
                        av.limpiar();
                        //ut.cargartablaventas(rv.ventastotalesTable);
                    } else if (validador.equals("1")) {
                        JOptionPane.showMessageDialog(null, "No hay producto en stock");
                    }

                }
            } catch (Exception e) {
                //System.out.println(e.getMessage());
            }
        }
        for (int i = 0; i < av.TotalProductosTabla.getRowCount(); i++) {
            try {
                String sql1 = "EXEC REGISTRAR_detalle '" + Integer.parseInt(av.facturaField.getText()) + "','" + av.TotalProductosTabla.getValueAt(i, 2) + "','" + av.TotalProductosTabla.getValueAt(i, 4) + "','" + av.TotalProductosTabla.getValueAt(i, 6) + "'";

                Statement st1 = con.createStatement();
                rs1 = st1.executeQuery(sql1);

                

            } catch (NumberFormatException | SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        
        try {
            if(rs1.next()){
            if(rs1.getString("Codigo").equals("0")){
                JOptionPane.showMessageDialog(null, "Venta Exitosa");
            }
        }
        } catch (HeadlessException | SQLException e) {
            System.out.println(e.getMessage());
        }
        
    }

    public void añadir() {
        avm = av.getData();
        String idcliente = avm.getCodigocliente();
        String nombrecliente = avm.getNombrecliente();
        String idproducto = avm.getCodigoproducto();
        String nombreproducto = avm.getNombreproducto();
        String cantidad = avm.getCantidad();
        String precio = avm.getPrecio();
        String total = avm.getTotal();

        DefaultTableModel modelo = (DefaultTableModel) av.TotalProductosTabla.getModel();
      

        //Sección 2
        Object[] fila = new Object[7];

        //Sección 3
        fila[0] = idcliente;
        fila[1] = nombrecliente;
        fila[2] = idproducto;
        fila[3] = nombreproducto;
        fila[4] = cantidad;
        fila[5] = precio;
        fila[6] = total;

        //Sección 4
        modelo.addRow(fila);
       // av.limpiar();
       av.CleanProductInfo();
    }

}
