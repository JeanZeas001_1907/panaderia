/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.CargarTablas;
import Vista.AñadirVentas;
import Vista.ConsultarVentas;
import Vista.Empleado;
import Vista.Ventas_emple;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Yader
 */
public class VentasControlador_empleado implements ActionListener {

    Ventas_emple Venemp;


    public VentasControlador_empleado(Ventas_emple Venemp) {
        super();
        this.Venemp = Venemp;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Añadir":
                AñadirVentas Añad = new AñadirVentas();
                Añad.setVisible(true);
                break;

            case "Consultar":
                ConsultarVentas Elim = new ConsultarVentas();
                Elim.setVisible(true);
                break;

            case "Regresar":
                Venemp.dispose();
                Empleado em = new Empleado();
                em.setVisible(true);

            case "Actualizar":
                CargarTablas CT = new CargarTablas();
                CT.CargarVentas(Venemp.TablaVentas);
                break;
           
        }
    }
}
