/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Vista.Clientes_empleado;
import Vista.Empleado;
import Vista.InicioSesion;
import Vista.InventarioEmpleado;
import Vista.Ventas_admin;
import Vista.Ventas_emple;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener; 

/**
 *
 * @author Paulino Zelaya
 */
public class EmpleadoControlador implements ActionListener {
    
    Empleado Em;
    
    public EmpleadoControlador(Empleado Em){
        super();
        this.Em = Em;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "Ventas":
                Ventas_emple V = new Ventas_emple();
                Em.dispose();
                V.setVisible(true);
                break;
                
            case "Inventario":
                InventarioEmpleado Inv = new InventarioEmpleado();
                Em.dispose();
                Inv.setVisible(true);
                break;
            
            case "ClientesEmp":
                Clientes_empleado Cliemp = new Clientes_empleado();
                Em.dispose();
                Cliemp.setVisible(true);
                break;    
                
            case "Cerrar Sesion":
                InicioSesion IS = new InicioSesion();
                IS.setVisible(true);
                Em.dispose();
                break;
        }
    }
}
