/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.ame_usuario_modelo;
import Vista.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import ConexionSQL.*;
import javax.swing.JOptionPane;


public class ame_usuario_controlador implements ActionListener{

    AñadirUsuario AñUs;
    ModificarUsuario MoUs;
    EliminarUsuario ElUs;
    ame_usuario_modelo ameUsmo;
    
    public ame_usuario_controlador(AñadirUsuario AñUs){
        super();
        this.AñUs = AñUs;
        this.ameUsmo = new ame_usuario_modelo();
    }
    
    public ame_usuario_controlador(ModificarUsuario MoUs){
        super();
        this.MoUs = MoUs;
        ameUsmo = new ame_usuario_modelo();
    }
    
    public ame_usuario_controlador(EliminarUsuario ElUs){
        super();
        this.ElUs = ElUs;
        ameUsmo = new ame_usuario_modelo();
    }

    public ame_usuario_modelo getAmeUsmo() {
        return ameUsmo;
    }

    public void setAmeUsmo(ame_usuario_modelo ameUsmo) {
        this.ameUsmo = ameUsmo;
    }
    
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "Añadir":
                add();
                    break;
            case "Xañadirusuario":
                AñUs.dispose();              
                break;
                
            case "Xeliminarusuario":
                ElUs.dispose();
                break;
                
            case "Buscar_ModificarUsuario":
                stm(); //stm = Search To Modify
                break;
                
            case "Xmodificarusuario":                
                MoUs.dispose();
                break;
            case "Modificar":
                Modify();
                break;
            case "Buscar_EliminarUsuario":
                search();
                break;
                
            case "Eliminar":
                dropuser();
                break;
        }
    }
    public void add()
    {
     
        conexionsql csql = new conexionsql();
        Connection con = csql.getConnection();
        ameUsmo = AñUs.getdata();
        
        
        try 
        {
                      
          CallableStatement cs = con.prepareCall("{call InsertarUsuario (?,?,?,?,?,?,?)}");
          cs.setString(1, ameUsmo.getNombre());
          cs.setString(2, ameUsmo.getApellido());
          cs.setString(3, ameUsmo.getTelefono());
          cs.setString(4, ameUsmo.getDireccion());
          cs.setString(5, ameUsmo.getUsuario());
          cs.setString(6, ameUsmo.getContraseña());
          cs.setString(7, ameUsmo.getTipo_rol()); 
          ResultSet rs  = cs.executeQuery();
          
          if (rs.next())
          {
              AñUs.lblConfirmation.setText(rs.getString(1));
              AñUs.limpiar();
          }
          
      
        }
        catch(SQLException e)
        {
            System.out.println(e.toString());
        }
        
    }
    
    public void stm()
            
    {
        
        conexionsql csql = new conexionsql();
        Connection con = csql.getConnection();
        ameUsmo = MoUs.getdata();
        
        try
        {
            CallableStatement cs  = con.prepareCall("{call SearchToModify (?)}");
            cs.setString(1, ameUsmo.getId());
            ResultSet rs = cs.executeQuery();
            
            if (rs.next())
            {
                String msg  = rs.getString(1);
               
                switch(msg)
                {
                    case "Usuario Encontrado":
                        ameUsmo.setUsuario(rs.getString(2));
                        ameUsmo.setContraseña(rs.getString(3));
                        ameUsmo.setTipo_rol(rs.getString(4));
                        ameUsmo.setNombre(rs.getString(5));
                        ameUsmo.setApellido(rs.getString(6));
                        ameUsmo.setTelefono(rs.getString(7));
                        ameUsmo.setDireccion(rs.getString(8));
                        ameUsmo.setMsg(msg);
                        MoUs.setdata(ameUsmo);
                        break;
                    case "Usuario Inexistente":
                        MoUs.msg.setText(msg);
                        break;
                }
            }
            
        }
        catch(SQLException e)
        {
        }
        
    }
    
   public void Modify()
           
   {
       
        conexionsql csql = new conexionsql();
        Connection con = csql.getConnection();
        ameUsmo = MoUs.GetDataToModify();
        
        try
        {
            CallableStatement cs = con.prepareCall("{call ModifyUser (?,?,?,?,?,?,?,?)}");
            cs.setString(1, ameUsmo.getId());
            cs.setString(2, ameUsmo.getUsuario());
            cs.setString(3, ameUsmo.getContraseña());
            cs.setString(4, ameUsmo.getTipo_rol());
            cs.setString(5, ameUsmo.getNombre());
            cs.setString(6, ameUsmo.getApellido());
            cs.setString(7,ameUsmo.getTelefono());
            cs.setString(8,ameUsmo.getDireccion());
            cs.execute();            
            ameUsmo.setMsg("Datos Actualizados Correctamente");
            MoUs.ConfirmationUpdate(ameUsmo);
            MoUs.limpiar();
        }
        
        catch(SQLException e)
        {}
        
       
   }
    
   public void search()
    {
        conexionsql csql = new conexionsql();
        Connection con = csql.getConnection();
        ameUsmo = ElUs.getdata();
        
        try {
        CallableStatement cs = con.prepareCall("{call BuscarUsuario (?)}");
        cs.setString(1, ameUsmo.getId());
        ResultSet rs = cs.executeQuery();
        
        if(rs.next())
        {
            String msg = rs.getString(1);
            switch (msg){
                
                case "Usuario Inexistente":
                    ameUsmo.setMsg(rs.getString(1));
                    ElUs.setdata(ameUsmo);
                    break;
               
                case "Usuario Encontrado":
                    ameUsmo.setMsg(rs.getString(1));
                    ameUsmo.setNombre(rs.getString(2));
                    ameUsmo.setApellido(rs.getString(3));
                    ameUsmo.setUsuario(rs.getString(4));
                    ameUsmo.setContraseña(rs.getString(5));
                    ElUs.setdata(ameUsmo);
                    break;

            }
        }
        
        }catch(Exception e)
        {
            System.out.println(e.toString());
        }
        
        
    }
    
    public void dropuser()
            
    {
        conexionsql csql = new conexionsql();
        Connection con = csql.getConnection();
        ameUsmo = ElUs.getdata() ;
        try{
        CallableStatement cs  = con.prepareCall("{call EliminarUsuario (?)}");
        cs.setString(1, ameUsmo.getId());
        ResultSet rs =  cs.executeQuery();
        
        if(rs.next())
        {
            JOptionPane.showMessageDialog(null, rs.getString(1));
            ElUs.limpiar();
        }
        
        }catch(Exception e)
        {
            System.out.println(e.toString());
        }
   }
}
