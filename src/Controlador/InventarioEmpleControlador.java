/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Vista.AñadirInventarioAdmin;
import Vista.EliminarInventarioAdmin;
import Vista.Empleado;
import Vista.InventarioEmpleado;
import Vista.ModificarInventarioAdmin;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Paulino Zelaya
 */
public class InventarioEmpleControlador implements ActionListener {
    
    InventarioEmpleado INADemp;
    
    public InventarioEmpleControlador(InventarioEmpleado INADemp){
        super();
        this.INADemp = INADemp;
    }
    

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "Regresar":
                INADemp.dispose();
                Empleado em = new Empleado();
                em.setVisible(true);
        }
    }
    
}
