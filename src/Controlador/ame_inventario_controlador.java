/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.ame_inventario_modelo;
import Vista.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import ConexionSQL.*;
import java.sql.*;
import javax.swing.JOptionPane;

public class ame_inventario_controlador implements ActionListener {

    AñadirInventarioAdmin AñIn;
    ModificarInventarioAdmin MoIn;
    EliminarInventarioAdmin ElIn;
    ame_inventario_modelo ameInmo;
    conexionsql csql = new conexionsql();
    Connection con = csql.getConnection();
    InventarioAdministrador IA = new InventarioAdministrador();

    public ame_inventario_controlador(AñadirInventarioAdmin AñIn) {
        super();
        this.AñIn = AñIn;
        ameInmo = new ame_inventario_modelo();
    }

    public ame_inventario_controlador(ModificarInventarioAdmin MoIn) {
        super();
        this.MoIn = MoIn;
        ameInmo = new ame_inventario_modelo();
    }

    public ame_inventario_controlador(EliminarInventarioAdmin ElIn) {
        super();
        this.ElIn = ElIn;
        ameInmo = new ame_inventario_modelo();
    }

    public ame_inventario_modelo getAmeInmo() {
        return ameInmo;
    }

    public void setAmeInmo(ame_inventario_modelo ameInmo) {
        this.ameInmo = ameInmo;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {

            case "Añadir":
                AddProduct();
                break;

            case "Xañadirinventario":
                AñIn.dispose();
                break;

            case "BackToInventarioAdmin":
                ElIn.dispose();
                break;
            case "Buscar":
                SearchProduct();
                break;
            case "Eliminar":
                Eliminar();
                break;
            case "Xmodificarinventario":
                MoIn.dispose();
                break;
            case "Buscar_modificar":
                search_modify();
                break;
            case "Modificar":
                modificar();
                break;

        }
    }

    public void AddProduct() {
        ameInmo = AñIn.getinfo();

        try {
            if (ameInmo.getNombreProducto().isEmpty() || ameInmo.getPrecioVenta().isEmpty() || ameInmo.getPrecioCompra().isEmpty() || ameInmo.getCanTotal().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Por favor llenar los campos vacios");
            } else {
                CallableStatement cs = con.prepareCall("{call AddProduct (?,?,?,?)}");

                cs.setString(1, ameInmo.getNombreProducto());
                cs.setString(2, ameInmo.getPrecioVenta());
                cs.setString(3, ameInmo.getPrecioCompra());
                cs.setString(4, ameInmo.getCanTotal());
                ResultSet rs = cs.executeQuery();
                if (rs.next()) {
                    JOptionPane.showMessageDialog(null, rs.getString(1));
                    AñIn.CleanForm();

                }

            }

        } catch (Exception e) {
            System.out.println(e.toString());
        }

    }

    public void SearchProduct() {
        ameInmo = ElIn.GetProductID();
        try {
            CallableStatement cs = con.prepareCall("{call SearchProduct (?)}");
            cs.setString(1, ameInmo.getId());
            ResultSet rs = cs.executeQuery();

            if (rs.next()) {
                switch (rs.getString(1)) {
                    case "Producto Encontrado":
                        ameInmo.setNombreProducto(rs.getString(3));
                        System.out.println(ameInmo.getNombreProducto());
                        ElIn.setdata(ameInmo);
                        break;
                    case "Producto No encontrado":
                        JOptionPane.showMessageDialog(null, rs.getString(1));
                        break;

                }
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
    
    public void Eliminar(){
        ameInmo = ElIn.GetProductID();
        
        try {
            CallableStatement cs = con.prepareCall("{call EliminarProducto (?)}");
            cs.setString(1, ameInmo.getId());
            ResultSet rs = cs.executeQuery();
            
            if(rs.next()){
                if(rs.getString("Message").equals("0")){
                    ElIn.lblmsg.setText("Eliminado Correctamente");
                }else{
                    ElIn.lblmsg.setText("Producto No Encontrado");
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void search_modify(){
        ameInmo = MoIn.getData();
        try {
            CallableStatement cs = con.prepareCall("{call SearchProduct (?)}");
            cs.setString(1, ameInmo.getId());
            ResultSet rs = cs.executeQuery();

            if (rs.next()) {
                switch (rs.getString(1)) {
                    case "Producto Encontrado":
                        ameInmo.setNombreProducto(rs.getString(3));
                        ameInmo.setPrecioCompra(rs.getString(4));
                        ameInmo.setPrecioVenta(rs.getString(5));
                        System.out.println(ameInmo.getNombreProducto());
                        MoIn.setData(ameInmo);
                        break;
                    case "Producto No encontrado":
                        JOptionPane.showMessageDialog(null, rs.getString(1));
                        break;

                }
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
    
    public void modificar(){
        ameInmo = MoIn.getData();
        
        try {
            CallableStatement cs = con.prepareCall("{call ModificarInventario (?,?,?,?,?)}");
            cs.setString(1, ameInmo.getId());
            cs.setString(2, ameInmo.getNombreProducto());
            cs.setString(3, ameInmo.getPrecioCompra());
            cs.setString(4, ameInmo.getPrecioVenta());
            cs.setString(5, ameInmo.getCanTotal());
            
            ResultSet rs = cs.executeQuery();
            
            if(rs.next()){
                if(rs.getString("CODIGO").equals("0")){
                    MoIn.lblmsg.setText("Modificado Correctamente");
                    MoIn.CleanForm();
                }else{
                    MoIn.lblmsg.setText("Ocurrio un error");
                }
            }
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
