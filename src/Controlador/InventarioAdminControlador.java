/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Vista.Administrador;
import Vista.AñadirInventarioAdmin;
import Vista.EliminarInventarioAdmin;
import Vista.InventarioAdministrador;
import Vista.ModificarInventarioAdmin;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Paulino Zelaya
 */
public class InventarioAdminControlador implements ActionListener {
    
    InventarioAdministrador INAD;
    
    public InventarioAdminControlador(InventarioAdministrador INAD){
        super();
        this.INAD = INAD;
    }
    

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "Añadir":
                AñadirInventarioAdmin Añad = new AñadirInventarioAdmin();
                Añad.setVisible(true);
                break;
            
            case "Modificar":
                ModificarInventarioAdmin Mod = new ModificarInventarioAdmin();
                Mod.setVisible(true);
                break;
                
            case "Eliminar":
                EliminarInventarioAdmin Elim = new EliminarInventarioAdmin();
                Elim.setVisible(true);
                break;
                
            case "Regresar":                
                INAD.dispose();
                Administrador Ad = new Administrador();
                Ad.setVisible(true);
            case "Actualizar":
                INAD.UpdateProductTable();
                break;
                
                
        }
    }
    
}
