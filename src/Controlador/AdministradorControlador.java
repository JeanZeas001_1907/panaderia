/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Vista.Administrador;
import Vista.Clientes_administrador;
import Vista.InicioSesion;
import Vista.InventarioAdministrador;
import Vista.UsuarioAdministrador;
import Vista.Ventas_admin;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Paulino Zelaya
 */
public class AdministradorControlador implements ActionListener {
    
    Administrador Ad;
    
    public AdministradorControlador(Administrador Ad){
        super();
        this.Ad = Ad;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){   
            case "Usuario":
                UsuarioAdministrador Us = new UsuarioAdministrador();
                Ad.dispose();
                Us.setVisible(true); 
                break;
                
            case "Clientes":
                Clientes_administrador Cl = new Clientes_administrador();
                Ad.dispose();
                Cl.setVisible(true);
                break;
                
            case "Ventas":
                Ventas_admin V = new Ventas_admin();
                Ad.dispose();
                V.setVisible(true);
                break;
                
            case "Inventario":
                InventarioAdministrador Inv = new InventarioAdministrador();
                Ad.dispose();
                Inv.setVisible(true);
                break;
                
            case "Cerrar Sesion":
                InicioSesion IS = new InicioSesion();
                IS.setVisible(true);
                Ad.dispose();
                break;
        }           
    }
    
}
