/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Vista.*;
import Vista.EliminarCliente;
import Vista.ModificarCliente;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Modelo.CargarTablas;
/**
 *
 * @author Paulino Zelaya
 */
public class ClienteControlador_admin implements ActionListener {

    Clientes_administrador Cli;
    
    public ClienteControlador_admin (Clientes_administrador Cli){
        super();
        this.Cli = Cli;
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "Añadir":
                AñadirCliente Añad = new AñadirCliente();
                Añad.setVisible(true);
                break;
            
            case "Modificar":
                ModificarCliente Mod = new ModificarCliente();
                Mod.setVisible(true);
                break;
                
            case "Eliminar":
                EliminarCliente Elim = new EliminarCliente();
                Elim.setVisible(true);
                break;
                
            case "Regresar":
                Cli.dispose();
                Administrador Ad = new Administrador();
                Ad.setVisible(true);
                break;
                
                case "Actualizar":
                    CargarTablas CT = new CargarTablas();
                    CT.CargarClientes(Cli.tblClientesAdmin);
                break;
        }
    }
    
    
}
