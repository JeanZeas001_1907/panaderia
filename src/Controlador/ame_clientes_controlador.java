/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.ame_clientes_modelo;
import Vista.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import ConexionSQL.*;
import java.awt.HeadlessException;
import javax.swing.JOptionPane;
import java.sql.*;

public class ame_clientes_controlador implements ActionListener {

    AñadirCliente AñCli;
    ModificarCliente MoCli;
    EliminarCliente ElCli;
    ame_clientes_modelo ameClimo;
    conexionsql csql = new conexionsql();
    Connection con = csql.getConnection();
    Clientes_administrador ca = new Clientes_administrador();

    public ame_clientes_controlador(AñadirCliente AñCli) {
        super();
        this.AñCli = AñCli;
        ameClimo = new ame_clientes_modelo();
    }

    public ame_clientes_controlador(ModificarCliente MoCli) {
        super();
        this.MoCli = MoCli;
        ameClimo = new ame_clientes_modelo();
    }

    public ame_clientes_controlador(EliminarCliente ElCli) {
        super();
        this.ElCli = ElCli;
        ameClimo = new ame_clientes_modelo();
    }

    public ame_clientes_modelo getAmeClimo() {
        return ameClimo;
    }

    public void setAmeClimo(ame_clientes_modelo ameClimo) {
        this.ameClimo = ameClimo;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Añadir":
                addcustomer();
                break;

            case "Xañadircliente":
                AñCli.dispose();
                break;

            case "Buscar":
                SearchCustomer();
                break;
            case "Eliminar":
                DeleteCustomer();
                break;
            case "Xeliminarcliente":
                ElCli.dispose();
                break;
            case "Xmodificarcliente":
                MoCli.dispose();
                break;

            case "SearchToModify":
                SearchCustomerToModify();
                break;

            case "Modificar":

                ModifyCustomerData();
                break;

        }
    }

    public void addcustomer() {

        try {
            ameClimo = AñCli.getdata();
            CallableStatement cs = con.prepareCall("{call InsertarCliente (?,?,?,?)}");
            cs.setString(1, ameClimo.getNombre());
            cs.setString(2, ameClimo.getApellido());
            cs.setString(3, ameClimo.getTelefono());
            cs.setString(4, ameClimo.getDireccion());
            ResultSet rs = cs.executeQuery();
            if (rs.next()) {
                AñCli.lblwarning.setText(rs.getString(1));
                AñCli.CleanForm();

            }

        } catch (SQLException e) {
            System.out.println(e.toString());
        }

    }

    public void SearchCustomer() {
        ameClimo = ElCli.getdata();
        try {
            CallableStatement cs = con.prepareCall("SELECT * FROM VistaClientes where ID_CLIENTE = ?");
            cs.setString(1, ameClimo.getId());
            ResultSet rs = cs.executeQuery();

            if (rs.next()) {
                ameClimo.setNombre(rs.getString(2));
                ameClimo.setApellido(rs.getString(3));
                ElCli.setdata(ameClimo);
                ElCli.lblmsg.setText("");
            }else{
                ElCli.lblmsg.setText("Cliente no encontrado");
                ElCli.CleanForm( );
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }

    }

    public void DeleteCustomer() {

        ameClimo = ElCli.getdata();

        try {
            CallableStatement cs = con.prepareCall("{call DeleteCustomer (?)}");
            cs.setString(1, ameClimo.getId());
            ResultSet rs = cs.executeQuery();

            if (rs.next()) {
                if (rs.getInt(1)==0) {
                    ElCli.lblmsg.setText("Cliente Eliminado");
                    ElCli.CleanForm();
                    
                }
            }

        } catch (SQLException e) {
            System.out.println(e.toString());
        }

    }

    public void SearchCustomerToModify() {
        ameClimo = MoCli.GetDataToModify();

        if (ameClimo.getId().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Campo IDCliente Vacio");
        } else {

            try {
                CallableStatement cs = con.prepareCall("{call SearchCustomer (?)}");
                cs.setString(1, ameClimo.getId());
                ResultSet rs = cs.executeQuery();

                if (rs.next()) {
                    switch (rs.getString(1)) {
                        case "Cliente Encontrado":
                            ameClimo.setNombre(rs.getString(2));
                            ameClimo.setApellido(rs.getString(3));
                            ameClimo.setTelefono(rs.getString(4));
                            ameClimo.setDireccion(rs.getString(5));
                            MoCli.setdata(ameClimo);
                            break;
                        case "Cliente inexistente":
                            MoCli.cleanform();
                            MoCli.lblmsg.setText("Cliente no existe");
                            break;

                    }
                }
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
    }

    public void ModifyCustomerData() {
        ameClimo = MoCli.GetCustomerUpdate();
        try {

            if (ameClimo.getId().isEmpty() || ameClimo.getNombre().isEmpty() || ameClimo.getApellido().isEmpty() || ameClimo.getTelefono().isEmpty() || ameClimo.getDireccion().isEmpty()) {
                JOptionPane.showMessageDialog(null, "No es posible modificar informacion debido a campos vacios");
            } else {
                CallableStatement cs = con.prepareCall("{call ModifyCustomerInfo(?,?,?,?,?)}");
                cs.setString(1, ameClimo.getId());
                cs.setString(2, ameClimo.getNombre());
                cs.setString(3, ameClimo.getApellido());
                cs.setString(4, ameClimo.getTelefono());
                cs.setString(5, ameClimo.getDireccion());
                ResultSet rs = cs.executeQuery();

                if (rs.next()) {
                    JOptionPane.showMessageDialog(null, rs.getString(1));
                    MoCli.CleanAfterUpdate();
                }

            }
        } catch (HeadlessException | SQLException e) {
            System.out.println(e.toString());
        }
    }
}
