/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.CargarTablas;
import Vista.Administrador;
import Vista.AñadirVentas;
import Vista.ConsultarVentas;
import Vista.Ventas_admin;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VentasControlador_admin implements ActionListener{
    
    Ventas_admin Ven;

    public VentasControlador_admin(Ventas_admin Ven){
        super();
        this.Ven = Ven;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "Añadir":
                AñadirVentas Añad = new AñadirVentas();
                Añad.setVisible(true);
                break;
            
            case "Consultar":
                ConsultarVentas Elim = new ConsultarVentas();
                Elim.setVisible(true);
                break;
                
            case "Regresar":
                Ven.dispose();
                Administrador Ad = new Administrador();
                Ad.setVisible(true);
                
            case "Actualizar":
                CargarTablas CT = new CargarTablas();
                CT.CargarVentas(Ven.TablaVentas);
                break;
        }

    }
    
    
}
