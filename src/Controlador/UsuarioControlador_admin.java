/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Vista.Administrador;
import Vista.AñadirUsuario;
import Vista.EliminarUsuario;
import Vista.ModificarUsuario;
import Vista.UsuarioAdministrador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Paulino Zelaya
 */
public class UsuarioControlador_admin implements ActionListener{
    
    UsuarioAdministrador Us;
    
    public UsuarioControlador_admin(UsuarioAdministrador Us){
        super();
        this.Us = Us;
    }
    

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
             case "Añadir":
                AñadirUsuario Añad = new AñadirUsuario();
                Añad.setVisible(true);
                break;
            
            case "Modificar":
                ModificarUsuario Mod = new ModificarUsuario();
                Mod.setVisible(true);
                break;
                
            case "Eliminar":
                EliminarUsuario Elim = new EliminarUsuario();
                Elim.setVisible(true);
                break;
                
            case "Regresar":
                Administrador Ad = new Administrador();
                Us.dispose();
                Ad.setVisible(true);
            case "Actualizar":
                    Us.cargarusuarios();
                    break;
        }
    }
    
}
