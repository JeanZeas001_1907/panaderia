/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.CargarTablas;
import Vista.AñadirCliente;
import Vista.Clientes_empleado;
import Vista.EliminarCliente;
import Vista.Empleado;
import Vista.ModificarCliente;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Paulino Zelaya
 */
public class ClienteControlador_empleado implements ActionListener {

    Clientes_empleado Cliemp;

    public ClienteControlador_empleado(Clientes_empleado Cliemp) {
        super();
        this.Cliemp = Cliemp;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Añadir":
                AñadirCliente Añad = new AñadirCliente();
                Añad.setVisible(true);
                break;

            case "Modificar":
                ModificarCliente Mod = new ModificarCliente();
                Mod.setVisible(true);
                break;

            case "Eliminar":
                EliminarCliente Elim = new EliminarCliente();
                Elim.setVisible(true);
                break;

            case "Regresar":
                Cliemp.dispose();
                Empleado Em = new Empleado();
                Em.setVisible(true);
                break;

            case "Actualizar":
                CargarTablas CT = new CargarTablas();
                CT.CargarClientes(Cliemp.tblClientesAdmin);
        }
    }

}
