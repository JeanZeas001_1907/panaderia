/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Modelo.*;
import Controlador.*;
import javax.swing.ImageIcon;


public class ModificarUsuario extends javax.swing.JFrame {


    public ModificarUsuario() {
        initComponents();
        setcontroller();
        this.setLocationRelativeTo(null);
        this.setTitle("Modificar Usuario");
         setIconImage(new ImageIcon(getClass().getResource("../Recursos/bakery.png")).getImage());
    }
    
    public void limpiar(){
        this.idfield.setText("");
        this.usuarioField.setText("");
        this.contraseñaField.setText("");
        this.nombreField.setText("");
        this.apellidoField.setText("");
        this.telefonoField.setText("");
        this.direccionField.setText("");
    }
    
    public void setcontroller()
    {
        ame_usuario_controlador auc = new ame_usuario_controlador(this);
        this.buscarButton.addActionListener(auc);
        this.modificarButton.addActionListener(auc);
        this.cerrarButton.addActionListener(auc);
    }
    
    public ame_usuario_modelo getdata()     
    {
        ame_usuario_modelo aum =  new ame_usuario_modelo();
        if(this.idfield.getText().isEmpty())
        {
            this.msg.setText("Campo de ID vacio");            
            return null;
        }
        
        aum.setId(this.idfield.getText());
        return aum;
        
    }
    
    public void setdata (ame_usuario_modelo aum)
    {
        this.usuarioField.setText(aum.getUsuario());
        this.contraseñaField.setText(aum.getContraseña());
        this.nombreField.setText(aum.getNombre());
        this.apellidoField.setText(aum.getApellido());
        this.telefonoField.setText(aum.getTelefono());
        this.direccionField.setText(aum.getDireccion());
        this.jComboBox1.setSelectedItem(aum.getTipo_rol());
        this.msg.setText(aum.getMsg());
    }
    
    public ame_usuario_modelo GetDataToModify()
            
    {
        ame_usuario_modelo aum = new ame_usuario_modelo();
        if( usuarioField.getText().isEmpty()||contraseñaField.getText().isEmpty()||nombreField.getText().isEmpty()||apellidoField.getText().isEmpty()||telefonoField.getText().isEmpty()||direccionField.getText().isEmpty())
        {
            msg.setText("Por favor llenar campos vacios");
            return null;
        }
        aum.setId(this.idfield.getText());
        aum.setUsuario(this.usuarioField.getText());
        aum.setContraseña(this.contraseñaField.getText());
        aum.setTipo_rol(this.jComboBox1.getSelectedItem().toString());
        aum.setNombre(this.nombreField.getText());
        aum.setApellido(this.apellidoField.getText());
        aum.setTelefono(this.telefonoField.getText());
        aum.setDireccion(this.direccionField.getText());
        return aum;
        
    }

     public void ConfirmationUpdate (ame_usuario_modelo aum)
     {
         this.msg.setText(aum.getMsg());
     }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        idfield = new javax.swing.JTextField();
        buscarButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        modificarButton = new javax.swing.JButton();
        nombreField = new javax.swing.JTextField();
        apellidoField = new javax.swing.JTextField();
        telefonoField = new javax.swing.JTextField();
        direccionField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        cerrarButton = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        contraseñaField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        usuarioField = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox<>();
        msg = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(idfield, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 30, 40, 25));

        buscarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/search.png"))); // NOI18N
        buscarButton.setText("Buscar");
        buscarButton.setActionCommand("Buscar_ModificarUsuario");
        buscarButton.setBorderPainted(false);
        buscarButton.setContentAreaFilled(false);
        getContentPane().add(buscarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 20, -1, -1));

        jLabel2.setText("Nombre");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 170, -1, -1));

        jLabel3.setText("Apellido");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, -1, -1));

        jLabel4.setText("Teléfono");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 230, -1, 20));

        jLabel5.setText("Dirección");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 270, -1, -1));

        modificarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/Modify.png"))); // NOI18N
        modificarButton.setText("Modificar");
        modificarButton.setBorderPainted(false);
        modificarButton.setContentAreaFilled(false);
        modificarButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        modificarButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getContentPane().add(modificarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 290, 80, 80));
        getContentPane().add(nombreField, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 170, 150, 25));
        getContentPane().add(apellidoField, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 200, 150, 25));
        getContentPane().add(telefonoField, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 230, 150, 25));
        getContentPane().add(direccionField, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 260, 150, 25));

        jLabel6.setText("ID:");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, -1, 20));

        cerrarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/back.png"))); // NOI18N
        cerrarButton.setActionCommand("Xmodificarusuario");
        cerrarButton.setBorderPainted(false);
        cerrarButton.setContentAreaFilled(false);
        getContentPane().add(cerrarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 0, -1, -1));

        jLabel7.setText("Tipo de Rol");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, -1, -1));

        jLabel8.setText("Contraseña");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, -1, -1));
        getContentPane().add(contraseñaField, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 100, 150, 25));

        jLabel9.setText("Usuario");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, -1, -1));
        getContentPane().add(usuarioField, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 150, 25));

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Administrador", "Empleado" }));
        jComboBox1.setToolTipText("");
        getContentPane().add(jComboBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 140, 150, -1));

        msg.setForeground(new java.awt.Color(255, 0, 0));
        msg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        getContentPane().add(msg, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 380, 200, 25));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/Paulino's WallPaper.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(-7, -7, 340, 430));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ModificarUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ModificarUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ModificarUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ModificarUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ModificarUsuario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField apellidoField;
    private javax.swing.JButton buscarButton;
    private javax.swing.JButton cerrarButton;
    private javax.swing.JTextField contraseñaField;
    private javax.swing.JTextField direccionField;
    private javax.swing.JTextField idfield;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JButton modificarButton;
    public javax.swing.JLabel msg;
    private javax.swing.JTextField nombreField;
    private javax.swing.JTextField telefonoField;
    private javax.swing.JTextField usuarioField;
    // End of variables declaration//GEN-END:variables
}
