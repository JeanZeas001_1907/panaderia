/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Controlador.*;
import Modelo.*;
import javax.swing.ImageIcon;

public class ModificarCliente extends javax.swing.JFrame {


    public ModificarCliente() {
        initComponents();
        setcontroller();
        this.setLocationRelativeTo(null);
        this.setTitle("Modificar Cliente");
         setIconImage(new ImageIcon(getClass().getResource("../Recursos/bakery.png")).getImage());
    }
public void setcontroller()
{
    ame_clientes_controlador acc = new ame_clientes_controlador(this);
    this.buscarButton.addActionListener(acc);
    this.modificarButton.addActionListener(acc);
    this.cerrarButton.addActionListener(acc);
}

public ame_clientes_modelo GetDataToModify()
{
    ame_clientes_modelo acm = new ame_clientes_modelo();
    
    acm.setId(this.idfield.getText());
    return acm;
}

public void setdata (ame_clientes_modelo acm)

{
    this.nombreField.setText(acm.getNombre());
    this.apellidoField.setText(acm.getApellido());
    this.telefonoField.setText(acm.getTelefono());
    this.direccionField.setText(acm.getDireccion());
    
}

public void cleanform ()
{
    this.nombreField.setText("");
    this.apellidoField.setText("");
    this.telefonoField.setText("");
    this.direccionField.setText("");   
    
}

public ame_clientes_modelo GetCustomerUpdate()

{
    ame_clientes_modelo acm = new ame_clientes_modelo();
     acm.setId(this.idfield.getText());
     acm.setNombre(this.nombreField.getText());
     acm.setApellido(this.apellidoField.getText());
     acm.setTelefono(this.telefonoField.getText());
     acm.setDireccion(this.direccionField.getText());
     return acm;
}
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        idfield = new javax.swing.JTextField();
        buscarButton = new javax.swing.JButton();
        Jlabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        nombreField = new javax.swing.JTextField();
        apellidoField = new javax.swing.JTextField();
        telefonoField = new javax.swing.JTextField();
        direccionField = new javax.swing.JTextField();
        modificarButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        cerrarButton = new javax.swing.JButton();
        lblmsg = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(idfield, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 50, 40, 25));

        buscarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/search.png"))); // NOI18N
        buscarButton.setText("Buscar");
        buscarButton.setActionCommand("SearchToModify");
        buscarButton.setBorderPainted(false);
        buscarButton.setContentAreaFilled(false);
        getContentPane().add(buscarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 50, 100, -1));

        Jlabel1.setForeground(new java.awt.Color(255, 255, 255));
        Jlabel1.setText("Nombre");
        getContentPane().add(Jlabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 50, 20));

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Apellido");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, -1, -1));

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Teléfono");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, -1, -1));

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Dirección");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 210, -1, -1));
        getContentPane().add(nombreField, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 90, 150, 25));
        getContentPane().add(apellidoField, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 130, 150, 25));
        getContentPane().add(telefonoField, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 170, 150, 25));
        getContentPane().add(direccionField, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 210, 150, 25));

        modificarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/Modify.png"))); // NOI18N
        modificarButton.setText("Modificar");
        modificarButton.setBorderPainted(false);
        modificarButton.setContentAreaFilled(false);
        modificarButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        modificarButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getContentPane().add(modificarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 250, 80, -1));

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("ID:");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 60, -1, -1));

        cerrarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/back.png"))); // NOI18N
        cerrarButton.setActionCommand("Xmodificarcliente");
        cerrarButton.setBorderPainted(false);
        cerrarButton.setContentAreaFilled(false);
        cerrarButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getContentPane().add(cerrarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 0, -1, -1));

        lblmsg.setBackground(new java.awt.Color(204, 0, 0));
        getContentPane().add(lblmsg, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 350, 200, 20));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/Paulino's WallPaper.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 270, 390));

        pack();
    }// </editor-fold>//GEN-END:initComponents
public void CleanAfterUpdate()
{
    this.nombreField.setText("");
    this.apellidoField.setText("");
    this.telefonoField.setText("");
    this.direccionField.setText("");   
    this.idfield.setText("");
}

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ModificarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ModificarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ModificarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ModificarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ModificarCliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Jlabel1;
    private javax.swing.JTextField apellidoField;
    private javax.swing.JButton buscarButton;
    private javax.swing.JButton cerrarButton;
    private javax.swing.JTextField direccionField;
    private javax.swing.JTextField idfield;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    public javax.swing.JLabel lblmsg;
    private javax.swing.JButton modificarButton;
    private javax.swing.JTextField nombreField;
    private javax.swing.JTextField telefonoField;
    // End of variables declaration//GEN-END:variables
}
