/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ClienteControlador_admin;
import Modelo.*;
import javax.swing.ImageIcon;

public class Clientes_administrador extends javax.swing.JFrame {
CargarTablas CT = new CargarTablas();
    public Clientes_administrador() {
        initComponents();
        setController();
        CT.CargarClientes(tblClientesAdmin);
        CT.ajustarcolumnas(tblClientesAdmin);
        this.setLocationRelativeTo(null);
        this.setTitle("Clientes Administrador");
         setIconImage(new ImageIcon(getClass().getResource("../Recursos/bakery.png")).getImage());
        
    }
    
    public void setController(){
        ClienteControlador_admin CC = new ClienteControlador_admin(this);
        añadirButton.addActionListener(CC);
        modificarButton.addActionListener(CC);
        eliminarButton.addActionListener(CC);
        regresarButton.addActionListener(CC);
        this.actualizarButton.addActionListener(CC);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblClientesAdmin = new javax.swing.JTable();
        añadirButton = new javax.swing.JButton();
        actualizarButton = new javax.swing.JButton();
        modificarButton = new javax.swing.JButton();
        eliminarButton = new javax.swing.JButton();
        regresarButton = new javax.swing.JButton();
        ShowInactives = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tblClientesAdmin.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "IDCliente", "Nombre", "Apellido", "Celular", "Direccion", "Estado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblClientesAdmin);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 250, 800, 250));

        añadirButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/AgregarCliente.png"))); // NOI18N
        añadirButton.setText("Añadir");
        añadirButton.setBorderPainted(false);
        añadirButton.setContentAreaFilled(false);
        añadirButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        añadirButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getContentPane().add(añadirButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 100, -1, -1));

        actualizarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/UpdateIcon.png"))); // NOI18N
        actualizarButton.setText("Actualizar");
        actualizarButton.setBorderPainted(false);
        actualizarButton.setContentAreaFilled(false);
        actualizarButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        actualizarButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getContentPane().add(actualizarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 100, -1, -1));

        modificarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/Modify.png"))); // NOI18N
        modificarButton.setText("Modificar");
        modificarButton.setBorderPainted(false);
        modificarButton.setContentAreaFilled(false);
        modificarButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        modificarButton.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        modificarButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getContentPane().add(modificarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 100, -1, -1));

        eliminarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/Eliminar.png"))); // NOI18N
        eliminarButton.setText("Eliminar");
        eliminarButton.setBorderPainted(false);
        eliminarButton.setContentAreaFilled(false);
        eliminarButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        eliminarButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getContentPane().add(eliminarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 100, -1, -1));

        regresarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/Regresar.png"))); // NOI18N
        regresarButton.setActionCommand("Regresar");
        regresarButton.setBorderPainted(false);
        regresarButton.setContentAreaFilled(false);
        getContentPane().add(regresarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 80, -1));

        ShowInactives.setText("Mostrar Inactivos");
        ShowInactives.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ShowInactivesActionPerformed(evt);
            }
        });
        getContentPane().add(ShowInactives, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 220, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/Paulino's WallPaper.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 810, 500));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ShowInactivesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ShowInactivesActionPerformed
        if(ShowInactives.isSelected())
        {
            CT.CargarClientesInactivos(tblClientesAdmin);
        }
        else
                {
                 CT.CargarClientes(tblClientesAdmin);
                }
    }//GEN-LAST:event_ShowInactivesActionPerformed


    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Clientes_administrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Clientes_administrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Clientes_administrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Clientes_administrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Clientes_administrador().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox ShowInactives;
    private javax.swing.JButton actualizarButton;
    private javax.swing.JButton añadirButton;
    private javax.swing.JButton eliminarButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton modificarButton;
    private javax.swing.JButton regresarButton;
    public javax.swing.JTable tblClientesAdmin;
    // End of variables declaration//GEN-END:variables
}
