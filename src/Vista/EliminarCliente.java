/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Modelo.*;
import Controlador.*;
import javax.swing.ImageIcon;

public class EliminarCliente extends javax.swing.JFrame {

 
    public EliminarCliente() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setTitle("Eliminar Cliente");
        setcontroller();
         setIconImage(new ImageIcon(getClass().getResource("../Recursos/bakery.png")).getImage());
        
    }

    public void setcontroller()
    {
        ame_clientes_controlador deletecustomer  = new ame_clientes_controlador(this);
        this.buscarButton.addActionListener(deletecustomer);
        this.eliminarButton.addActionListener(deletecustomer);
        this.cerrarButton.addActionListener(deletecustomer);
        
    }
    public void CleanForm()
    {
        this.idField.setText("");
        this.nombreField.setText("");
        this.apellidoField.setText("");
    }
    
    public ame_clientes_modelo getdata ()
    {
        ame_clientes_modelo acm  = new ame_clientes_modelo();
        
        if(this.idField.getText().isEmpty())
        {
            this.lblmsg.setText("Campo de ID cliente Vacio");
            return null;
        }
        acm.setId(this.idField.getText());
        
        return acm;
    }
    
    public void setdata ( ame_clientes_modelo acm)
    {
        this.nombreField.setText(acm.getNombre());
        this.apellidoField.setText(acm.getApellido());
        
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        idField = new javax.swing.JTextField();
        nombreField = new javax.swing.JTextField();
        apellidoField = new javax.swing.JTextField();
        eliminarButton = new javax.swing.JButton();
        cerrarButton = new javax.swing.JButton();
        buscarButton = new javax.swing.JButton();
        lblmsg = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("ID:");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 80, -1, -1));

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Nombre");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, -1, -1));

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Apellido");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, -1, -1));
        getContentPane().add(idField, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 80, 40, 25));
        getContentPane().add(nombreField, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 120, 110, 25));
        getContentPane().add(apellidoField, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 160, 110, 25));

        eliminarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/Eliminar.png"))); // NOI18N
        eliminarButton.setText("Eliminar");
        eliminarButton.setBorderPainted(false);
        eliminarButton.setContentAreaFilled(false);
        eliminarButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        eliminarButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getContentPane().add(eliminarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 190, -1, -1));

        cerrarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/back.png"))); // NOI18N
        cerrarButton.setActionCommand("Xeliminarcliente");
        cerrarButton.setBorderPainted(false);
        cerrarButton.setContentAreaFilled(false);
        getContentPane().add(cerrarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 0, -1, -1));

        buscarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/search.png"))); // NOI18N
        buscarButton.setText("Buscar");
        buscarButton.setBorderPainted(false);
        buscarButton.setContentAreaFilled(false);
        getContentPane().add(buscarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, -1, -1));

        lblmsg.setForeground(new java.awt.Color(255, 0, 0));
        lblmsg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        getContentPane().add(lblmsg, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 290, 150, 25));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/Paulino's WallPaper.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(-6, -6, 280, 400));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EliminarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EliminarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EliminarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EliminarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EliminarCliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField apellidoField;
    private javax.swing.JButton buscarButton;
    private javax.swing.JButton cerrarButton;
    private javax.swing.JButton eliminarButton;
    private javax.swing.JTextField idField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    public javax.swing.JLabel lblmsg;
    private javax.swing.JTextField nombreField;
    // End of variables declaration//GEN-END:variables
}
