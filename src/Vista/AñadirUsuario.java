/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Controlador.*;
import Modelo.ame_usuario_modelo;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class AñadirUsuario extends javax.swing.JFrame {

    /**
     * Creates new form AñadirUsuario
     */
    public AñadirUsuario() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setTitle("Añadir Usuario");
         controlador();
          setIconImage(new ImageIcon(getClass().getResource("../Recursos/bakery.png")).getImage());
       
    }
    
    public void limpiar(){
        this.usuarioField.setText("");
        this.txtpassfield.setText("");
        this.nombreField.setText("");
        this.apellidoField.setText("");
        this.telefonoField.setText("");
        this.direccionField.setText("");       
    }
public void controlador() {
        ame_usuario_controlador u = new ame_usuario_controlador(this);
        añadirButton.addActionListener(u);
        cerrarButton.addActionListener(u);
    }

public ame_usuario_modelo getdata()
{
    ame_usuario_modelo aum  = new ame_usuario_modelo();
    if(this.nombreField.getText().isEmpty()||this.apellidoField.getText().isEmpty()||this.telefonoField.getText().isEmpty()||this.direccionField.getText().isEmpty()||this.usuarioField.getText().isEmpty()||this.txtpassfield.getPassword().equals(null))
    {
        JOptionPane.showMessageDialog(null, "Porfavor llena los campos vacios");
        return null;
    }
    aum.setNombre(this.nombreField.getText());
    aum.setApellido(this.apellidoField.getText());
    aum.setTelefono(this.telefonoField.getText());
    aum.setDireccion(this.direccionField.getText());
    aum.setUsuario(this.usuarioField.getText());
    aum.setContraseña(String.valueOf(this.txtpassfield.getPassword()));
    aum.setTipo_rol(this.rolComboBox.getSelectedItem().toString());
    
    return aum;
    
}
 
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        usuarioField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        nombreField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        apellidoField = new javax.swing.JTextField();
        añadirButton = new javax.swing.JButton();
        rolComboBox = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        direccionField = new javax.swing.JTextField();
        cerrarButton = new javax.swing.JButton();
        txtpassfield = new javax.swing.JPasswordField();
        lblConfirmation = new javax.swing.JLabel();
        telefonoField = new javax.swing.JFormattedTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(300, 400));
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(300, 436));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Usuario");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Contraseña");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, -1, -1));
        getContentPane().add(usuarioField, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 30, 150, 30));

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Tipo de Rol");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, -1, -1));

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Nombre");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, -1, -1));
        getContentPane().add(nombreField, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 150, 150, 30));

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Apellido");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, -1, -1));
        getContentPane().add(apellidoField, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 190, 150, 30));

        añadirButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/AgregarCliente.png"))); // NOI18N
        añadirButton.setText("Añadir");
        añadirButton.setBorderPainted(false);
        añadirButton.setContentAreaFilled(false);
        añadirButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        añadirButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        añadirButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                añadirButtonActionPerformed(evt);
            }
        });
        getContentPane().add(añadirButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 310, -1, -1));

        rolComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Administrador", "Empleado" }));
        rolComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rolComboBoxActionPerformed(evt);
            }
        });
        getContentPane().add(rolComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 110, 150, 20));

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Teléfono");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 230, -1, -1));

        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Dirección");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 270, -1, -1));
        getContentPane().add(direccionField, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 270, 150, 30));

        cerrarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/back.png"))); // NOI18N
        cerrarButton.setActionCommand("Xañadirusuario");
        cerrarButton.setBorderPainted(false);
        cerrarButton.setContentAreaFilled(false);
        getContentPane().add(cerrarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 0, -1, -1));

        txtpassfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpassfieldActionPerformed(evt);
            }
        });
        getContentPane().add(txtpassfield, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 70, 150, 30));

        lblConfirmation.setForeground(new java.awt.Color(255, 0, 0));
        getContentPane().add(lblConfirmation, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 400, 310, 20));

        try {
            telefonoField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        getContentPane().add(telefonoField, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 230, 150, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/Paulino's WallPaper.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 330, 440));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rolComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rolComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rolComboBoxActionPerformed

    private void txtpassfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpassfieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpassfieldActionPerformed

    private void añadirButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_añadirButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_añadirButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AñadirUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AñadirUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AñadirUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AñadirUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AñadirUsuario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField apellidoField;
    private javax.swing.JButton añadirButton;
    private javax.swing.JButton cerrarButton;
    private javax.swing.JTextField direccionField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    public javax.swing.JLabel lblConfirmation;
    private javax.swing.JTextField nombreField;
    private javax.swing.JComboBox<String> rolComboBox;
    private javax.swing.JFormattedTextField telefonoField;
    private javax.swing.JPasswordField txtpassfield;
    private javax.swing.JTextField usuarioField;
    // End of variables declaration//GEN-END:variables
}
