USE MASTER
GO

CREATE LOGIN PANADERIA WITH PASSWORD = N'1234'
GO

USE PANADERIA
GO

DROP USER Isela
GO

CREATE USER Isela FOR LOGIN PANADERIA
GO

--=======================================================================================--
CREATE ROLE db_select
go

ALTER ROLE db_select ADD MEMBER Isela
GO

GRANT SELECT ON SCHEMA::dbo TO db_select
GO
--=======================================================================================--
CREATE ROLE db_insert
GO

ALTER ROLE db_insert ADD MEMBER Isela
GO

GRANT INSERT ON SCHEMA::dbo TO db_insert
GO
--=======================================================================================--
CREATE ROLE db_update
go

ALTER ROLE db_update ADD MEMBER Isela
GO

GRANT UPDATE ON SCHEMA::dbo TO db_update
GO
--=======================================================================================--
CREATE ROLE db_delete
GO

ALTER ROLE db_delete ADD MEMBER Isela
GO

GRANT DELETE ON SCHEMA::dbo TO db_delete
GO
--=======================================================================================--
CREATE ROLE db_exec
GO

ALTER ROLE db_exec ADD MEMBER Isela

GRANT EXECUTE ON SCHEMA::dbo TO db_exec
GO  