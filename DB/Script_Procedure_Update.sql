--MODIFICAR USUARIO--
DROP PROCEDURE IF EXISTS ModifyUser
GO

Create Procedure ModifyUser
@idusuario int,
@Usuario varchar(100),
@Pass varchar(100),
@Rol varchar(100),
@Nombre varchar(100),
@Apellido varchar(100),
@Tel varchar(50),
@Direccion varchar(200)
as

update USUARIO 
Set
NOMBRE_USUARIO = @Usuario,
CONTRASENA = HASHBYTES('SHA2_256',@Pass),
TIPO_ROL = @Rol
Where ID_USUARIO = @idusuario

Declare @idPersona int
set @idPersona = (select ID_PERSONA FROM USUARIO WHERE ID_USUARIO = @idusuario)

Update PERSONA
Set
NOMBRE  = @Nombre,
APELLIDO = @Apellido,
CELULAR  = @Tel,
DIRECCION = @Direccion
where ID_PERSONA = @idPersona
go

--MODIFICAR CLIENTE--
Create Procedure ModifyCustomerInfo
@idCliente int,
@Nombre varchar(100),
@Apellido varchar (100),
@Tel varchar(50),
@Direccion varchar(100)
as

if exists (Select  * from CLIENTE where ID_CLIENTE = @idCliente)
	begin
		Declare @idpersona int
		Set @idpersona  = (SELECT ID_PERSONA FROM CLIENTE WHERE ID_CLIENTE = @idCliente)   

		UPDATE PERSONA
		SET 
		NOMBRE = @Nombre,
		APELLIDO = @Apellido,
		CELULAR  = @Tel,
		DIRECCION = @Direccion
		Where ID_PERSONA = @idpersona

		Select 'Cliente Modificado'
	end
else
	begin
		Select 'Cliente No Modificado'
	end
go

--MODIFICAR INVENTARIO
DROP PROCEDURE IF EXISTS ModificarInventario
GO

CREATE PROCEDURE ModificarInventario
@ID INT,
@NOMBRE VARCHAR(100),
@PRECIO_COMPRA FLOAT,
@PRECIO_VENTA FLOAT,
@CANTIDAD INT
AS
BEGIN
	IF EXISTS (SELECT * FROM INVENTARIO WHERE ID_PRODUCTO = @ID)
	BEGIN
	UPDATE INVENTARIO
	SET NOMBRE_PRODUCTO = @NOMBRE,
		COSTO_FABRICACION = @PRECIO_COMPRA,
		PRECIO = @PRECIO_VENTA
		WHERE ID_PRODUCTO = @ID

	UPDATE STOCK
	SET CANTIDAD_TOTAL = CANTIDAD_TOTAL + @CANTIDAD
		WHERE ID_PRODUCTO = @ID

			UPDATE STOCK
			SET CANTIDAD_DISPONIBLE = CANTIDAD_TOTAL - CANTIDAD_VENDIDA
				WHERE ID_PRODUCTO = @ID

	SELECT '0' AS 'CODIGO'
	END
	ELSE
		BEGIN
		SELECT '1' AS 'CODIGO'
		END
END
GO
