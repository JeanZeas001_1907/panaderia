CREATE TABLE PERSONA(
ID_PERSONA INT IDENTITY (1,1),
NOMBRE VARCHAR(50),
APELLIDO VARCHAR(50),
CELULAR VARCHAR(30),
DIRECCION VARCHAR(100),
CONSTRAINT PK_ID_PER PRIMARY KEY (ID_PERSONA)
)
GO

CREATE TABLE USUARIO(
ID_USUARIO INT IDENTITY (1,1),
NOMBRE_USUARIO VARCHAR(50),
CONTRASENA VARCHAR(50),
ID_PERSONA INT,
TIPO_ROL VARCHAR(30),
CONSTRAINT PK_ID PRIMARY KEY (ID_USUARIO),
CONSTRAINT FK_IDPERSON FOREIGN KEY (ID_PERSONA) REFERENCES PERSONA (ID_PERSONA)
)
GO

CREATE TABLE CLIENTE(
ID_CLIENTE INT IDENTITY (1,1),
ID_PERSONA INT,
ESTADO INT,
CONSTRAINT PK_IDCLIENT PRIMARY KEY (ID_CLIENTE),
CONSTRAINT FK_IDPERSO FOREIGN KEY (ID_PERSONA) REFERENCES PERSONA (ID_PERSONA)
)
GO

CREATE TABLE VENTAS(
ID_VENTAS INT IDENTITY (1,1),
FECHA DATE,
ID_CLIENTE INT,
ID_USUARIO INT,
CONSTRAINT PK_IDVENT PRIMARY KEY (ID_VENTAS),
CONSTRAINT FR_IDUSUARIO FOREIGN KEY (ID_USUARIO) REFERENCES USUARIO (ID_USUARIO),
CONSTRAINT FK_IDCLIENT FOREIGN KEY (ID_CLIENTE) REFERENCES CLIENTE (ID_CLIENTE)
)
GO

CREATE TABLE PROVEEDOR(
ID_PROVEEDOR INT IDENTITY (1,1),
ID_PERSONA INT,
CONSTRAINT PK_IDPROVEE PRIMARY KEY (ID_PROVEEDOR),
CONSTRAINT FK_IDPERS FOREIGN KEY (ID_PERSONA) REFERENCES PERSONA (ID_PERSONA)
)
GO

CREATE TABLE INVENTARIO(
ID_PRODUCTO INT IDENTITY (1,1),
NOMBRE_PRODUCTO VARCHAR(50),
PRECIO FLOAT,
PRECIO_COMPRA FLOAT,
ID_PROVEEDOR INT,
CONSTRAINT PK_IDPRODUC PRIMARY KEY (ID_PRODUCTO),
CONSTRAINT FK_IDPROV FOREIGN KEY (ID_PROVEEDOR) REFERENCES PROVEEDOR (ID_PROVEEDOR)
)
GO 

CREATE TABLE STOCK(
ID_PRODUCTO INT ,
CANTIDAD_TOTAL INT,
CANTIDAD_VENDIDA INT,
CANTIDAD_DISPONIBLE INT,
CONSTRAINT FK_IDPRODUC FOREIGN KEY (ID_PRODUCTO) REFERENCES INVENTARIO (ID_PRODUCTO)
)
GO

CREATE TABLE DETALLEVENTAS(
ID_DETALLE INT IDENTITY (1,1),
ID_VENTAS INT,
ID_PRODUCTO INT,
CANTIDAD_COMPRA INT,
TOTAL FLOAT,
CONSTRAINT PK_IDDETALLE PRIMARY KEY (ID_DETALLE),
CONSTRAINT FK_IDVENTAS FOREIGN KEY (ID_VENTAS) REFERENCES VENTAS (ID_VENTAS),
CONSTRAINT FK_IDPRODUCTO FOREIGN KEY (ID_PRODUCTO) REFERENCES INVENTARIO (ID_PRODUCTO)
)
GO