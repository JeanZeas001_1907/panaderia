---PROCEDIMIENTOS DE INSERCION--
--INSERTAR USUARIO--
Drop procedure IF EXISTS InsertarUsuario
GO

Create procedure InsertarUsuario
@Nombre varchar(100),
@Apellido varchar(100),
@Telefono varchar(100),
@Direccion varchar(200),
@User varchar(100),
@Pass varchar(100),
@Rol varchar(100)
as
if not exists (Select  * from USUARIO where NOMBRE_USUARIO = @User)
	begin
		Insert into PERSONA values (@Nombre,@Apellido,@Telefono,@Direccion)
		Declare @PersonID  int
		set @PersonID  = (SELECT  ID_PERSONA FROM PERSONA WHERE NOMBRE = @Nombre)

		insert into USUARIO values (@User,HASHBYTES('SHA2_256',@Pass),@PersonID,@Rol)
		select 'Usuario registrado exitosamente' as 'Confirmation'
	end
	else 
		begin
			Select 'Usuario Existente' as 'Confirmation'
		end
go


---INSERTAR CLIENTE--
Drop Procedure IF EXISTS InsertarCliente
GO

Create procedure InsertarCliente
@Nombre varchar(100),
@Apellido varchar(100),
@Telefono varchar(100),
@Direccion varchar(100)
as

if not exists(Select  * from PERSONA where NOMBRE = @Nombre and APELLIDO = @Apellido)
	begin 
		Insert into PERSONA values (@Nombre,@Apellido,@Telefono,@Direccion)
		declare @idpersona int
		set @idpersona  = (Select ID_PERSONA FROM PERSONA WHERE NOMBRE = @Nombre AND APELLIDO = @Apellido)
		Insert into CLIENTE values (@idpersona,0)
		select 'Cliente agregado correctamente' as 'Confirmation'
	end

else

	begin 
		Select 'Cliente ya existente'
	end

go

--INSERTAR INVENTARIO--
DROP PROCEDURE IF EXISTS AddProduct
GO

Create procedure AddProduct 
@NombreProducto varchar(100),
@PrecioCompra decimal(10,2),
@PrecioVenta decimal(10,2),
@Cantidad BigInt
as
IF not exists (Select * from INVENTARIO where NOMBRE_PRODUCTO = @NombreProducto)
	BEGIN 
		INSERT INTO INVENTARIO (NOMBRE_PRODUCTO,COSTO_FABRICACION,PRECIO) VALUES ( @NombreProducto,@PrecioCompra,@PrecioVenta)
		
		DECLARE @ID INT
		SET @ID = (SELECT ID_PRODUCTO FROM INVENTARIO WHERE NOMBRE_PRODUCTO = @NombreProducto)
		INSERT INTO STOCK (ID_PRODUCTO,CANTIDAD_TOTAL,CANTIDAD_VENDIDA,CANTIDAD_DISPONIBLE)
		VALUES(@ID,@Cantidad,'0',@Cantidad)
		SELECT 'Producto Agregado Correctamente' as 'Confirmation'
	END
Else
	BEGIN
		SELECT 'Producto Agregado Anteriormente' as 'Confirmation'
	END
go

--REGISTRAR VENTA
DROP PROCEDURE IF EXISTS REGISTRAR_VENTAS
GO

CREATE PROCEDURE REGISTRAR_VENTAS
	@ID_CLIENTE INT,
	@num_venta int
AS	
	
	BEGIN
	DECLARE @ESTADOVENTA VARCHAR(500)
	DECLARE @FECHA DATE
	SET @FECHA=GETDATE()
	SET @ESTADOVENTA = 'REALIZADO'

	INSERT INTO VENTAS(ID_CLIENTE,Num_Venta,FECHA)
	VALUES(@ID_CLIENTE,@NUM_VENTA,@FECHA)

	END

GO

--DETALLE DE VENTA
DROP PROCEDURE IF EXISTS REGISTRAR_detalle
GO

CREATE PROCEDURE REGISTRAR_detalle
@NUM_VENTA INT,
@ID_PRODUCTO INT,
@CANTIDAD INT,
@TOTAL INT
AS
BEGIN
	INSERT INTO DETALLEVENTAS(ID_VENTAS,ID_PRODUCTO,CANTIDAD_COMPRA,TOTAL)
	VALUES((SELECT ID_VENTAS FROM VENTAS WHERE Num_Venta=@NUM_VENTA),@ID_PRODUCTO,@CANTIDAD,@TOTAL)

	UPDATE STOCK
	SET CANTIDAD_VENDIDA = @CANTIDAD
	WHERE ID_PRODUCTO = @ID_PRODUCTO

	UPDATE STOCK 
	SET CANTIDAD_DISPONIBLE = CANTIDAD_TOTAL - CANTIDAD_VENDIDA
	WHERE ID_PRODUCTO = @ID_PRODUCTO

	SELECT 0 AS Codigo
END