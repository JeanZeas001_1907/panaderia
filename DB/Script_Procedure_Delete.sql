--ELIMINAR USUARIO--
DROP PROCEDURE IF EXISTS EliminarUsuario
GO

Create Procedure EliminarUsuario
@idusuario int
as
if exists (Select  * from USUARIO where ID_USUARIO = @idusuario)
begin
	Declare @idpersona int
	set @idpersona = (select ID_PERSONA from USUARIO where ID_PERSONA = @idusuario)

	delete from USUARIO where ID_USUARIO = @idusuario
	delete from PERSONA where ID_PERSONA = @idpersona
	Select 'Usuario Elimindo Correctamente' as 'Confirmation'

end
else
begin
	select 'Usuario no encontrado' as 'Confirmation'
end

go

--ELIMINAR CLIENTE--
DROP PROCEDURE IF EXISTS DeleteCustomer
GO

Create Procedure DeleteCustomer 
@idCliente int

as

IF EXISTS (SELECT* FROM CLIENTE WHERE ID_CLIENTE = @idCliente)
	BEGIN
		Declare @idPersona as int
		Set @idPersona = (Select ID_PERSONA from CLIENTE where ID_CLIENTE = @idCliente)
		Delete from CLIENTE where ID_CLIENTE = @idCliente
		Delete from PERSONA where ID_PERSONA = @idPersona
			select '0' as 'confirmation'
	END
		ELSE
		BEGIN
			select '1' as 'confirmation'
		END

go

--ELIMINAR PRODUCTO
DROP PROCEDURE IF EXISTS EliminarProducto
GO

CREATE PROCEDURE EliminarProducto
@ID INT
AS
BEGIN
	IF EXISTS(SELECT * FROM INVENTARIO WHERE ID_PRODUCTO = @ID)
	BEGIN
	    DELETE FROM STOCK WHERE ID_PRODUCTO = @ID
		DELETE FROM INVENTARIO WHERE ID_PRODUCTO = @ID

		SELECT '0' AS 'MESSAGE'
	END
	ELSE
		BEGIN
		SELECT '1' AS 'MESSAGE'
		END
END

